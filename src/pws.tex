%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sezione relativa al PWS
% Autore: Carlo Carraro
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Cos'è PWS}
Le reti 3GPP supportano il PWS. Il sistema \gls{pws} viene
utilizzato per allertare la popolazione in caso di eventi particolari: per
esempio nell'eventualità di terremoti, tsunami, uragani, incendi, viene
segnalato alla popolazione di evacuare l'area colpita. Inoltre il PWS
può essere utilizzato per altri scopi, per esempio per avvertire le persone in
caso di scomparsa di minori - eg. \gls{amber} alert -. Le notifiche di allerta
devono essere precise e consegnate in tempo utile per permettere alle persone
di prepararsi in modo adeguato all'evento. La consegna dei messaggi di
allerta varia da Paese a Paese, in
quanto questi possono avere requisiti diversi come specificato nel documento
tecnico 3GPP 22.268 \cite{3gppts22268}. Le reti 3GPP forniscono quindi
sistemi di allerta differenti a seconda del Paese; tuttavia questi sistemi
utilizzano un'architettura e procedure di segnalazione comuni.

\section{Breve storia di PWS}
Il 3GPP nel tempo ha standardizzato diverse versioni del sistema PWS in base
alle richieste pervenute dai Paesi. Il primo sistema di allerta ad essere
standardizzato è stato ETWS per il Giappone nella Release 8.
ETWS è stato progettato in base ai requisiti Giapponesi ponendo particolare
attenzione ai terremoti e agli tsunami. Un requisito importante di ETWS è che i
provider di allerta devono essere in grado di fornire ai dispositivi mobili
notifiche chiamate \textit{Primary} e \textit{Secondary Notifications}. Una
notifica primaria dovrebbe essere consegnata ai dispositivi degli utenti
entro 4 secondi anche in una situazione
di congestione in un'area di notifica (\textit{Notification Area}).
Per gli Stati Uniti d'America è stato standardizzato CMAS nella Release 9.
CMAS è stato progettato in base ai requisiti richiesti dalla \gls{fcc}. Uno tra
questi è che deve essere permesso che più notifiche vengano trasmesse in
contemporanea.
In base ai requisiti, CMAS supporta tre livelli di notifiche di allerta:
\textit{Presidenziale}, \textit{Minaccia Imminente} e \textit{Scomparsa di
Minore}. In seguito sono stati
standardizzati KPAS per la Korea del Sud nella
Release 10 e EU-ALERT per l'Europa nella Release 11. KPAS e EU-ALERT
utilizzano le stesse procedure definite per CMAS sia per quanto riguarda gli
apparati mobili che la rete. I messaggi di allerta per CMAS, KPAS e
EU-ALERT sono consegnati ai dispositivi mobili nello stesso modo. Tuttavia
KPAS e EU-ALERT hanno introdotto dei cambiamenti minori che non hanno avuto
un impatto significativo: sono stati semplicemente aggiunti dei nuovi
identificativi di messaggio sia per KPAS che per EU-ALERT. \`E bene
specificare che la ricezione delle notifiche PWS è opzionale nei dispositivi
mobili; tuttavia dipende dalla legislazione dei diversi Paesi l'obbligo di
imporre ai produttori di dispositivi di costruirli in modo tale che ricevano
le notifiche: per esempio i messaggi di livello presidenziale negli USA non
possono essere ignorati dal dispositivo, ovvero devono sempre essere
visualizzati senza che l'utente abbia la possibilità di deciderlo.

\begin{table}[htbp]
  \centering
  \begin{tabular}{@{} |c|c|c|c| @{}}
    \hline
    Warning System & Target Region            & Release    & Anno    \\
    \hline
    ETWS           & Japan                    & Release 8  & 2008 Q4 \\
    CMAS           & United States of America & Release 9  & 2009 Q4 \\
    KPAS           & South Korea              & Release 10 & 2011 Q1 \\
    EU-ALERT       & European countries       & Release 11 & 2012 Q3 \\
    \hline
  \end{tabular}
  \caption{Roadmap di Public Warning System}
  \label{tab:label}
\end{table}

\section{Architettura di PWS}
Nell'architettura di PWS sono contenuti il CBC e il CBE che
derivano dall'architettura del servizio denominato CBS
nelle reti GSM e UMTS. Come visto in precedenza esistono delle interfacce
tra CBC e Base Station Controller (BSC), tra CBC e Radio Network Controller (RNC) e tra CBC e la nuova
interfaccia con MME per la consegna dei messaggi di allerta sulla rete E-UTRAN.
La Figura \ref{fig:cbsarch} rappresenta l'architettura del sistema PWS sulle
reti del 3GPP.
Il CBE è la sorgente dei messaggi di allerta; è l'interfaccia usata dal
creatore del messaggio per formattare il messaggio e specificare la posizione
dei riceventi. Una volta definito, il messaggio viene inviato al CBC che mappa
l'area obiettivo su celle fisiche della rete e spedisce il messaggio broadcast
sulle reti specificate (GSM, 3G, LTE).
Il CBC gestisce i messaggi di allerta: alloca i numeri seriali per i messaggi
di allerta ed inizia la trasmissione broadcast inviando i messaggi a MME, RNC
e BSC. L'interfaccia di comunicazione tra CBC e MME è denominata
\textit{SBc}; tra CBC e RNC prende il nome di \textit{Iu-BC}; tra CBC e BSC è
denominata \textit{CBC-BSC}. Le interfacce SBc, Iu-BC e CBC-BSC
sono utilizzate per la consegna di messaggi di allerta e per i messaggi di
controllo.
Per quanto riguarda le reti 4G/LTE, possono essere coinvolti nella consegna dei
messaggi di allerta ai dispositivi mobili una o più MME ed una o più eNB. Lo
stesso vale per le reti GSM e le reti 3G, dove possono essere coinvolti
rispettivamente una o più BSC e BTS e uno o più RNC e NodeB.
MME inoltra i messaggi di allerta ricevuti dal CBC ai giusti eNodeB tramite
l'interfaccia S1. In seguito eNodeB trasmette i messaggi di allerta tramite
il control plane sull'interfaccia radio.

\section{Requisiti di PWS}
\label{sec:requirement}
Negli ultimi anni c'è stato un interesse generale da parte dei governi volto alla
possibilità di ricevere messaggi di allerta ed informazioni critiche che
riguardano disastri o emergenze di carattere pubblico. Questi messaggi devono
essere consegnati in maniera accurata, affidabile e nei tempi previsti.
Dall'esperienza sul campo maturata grazie a terremoti, tsunami, uragani e
incendi emerge come sia essenziale informare le persone in tempo reale cosicché
possano mettere in salvo loro stessi e le loro famiglie, prevenire ferite gravi, 
perdita di proprietà etc.
L'interesse rivolto ad aumentare l'affidabilità e la sicurezza di questi
messaggi ha portato allo sviluppo di PWS sulle reti cellulari del 3GPP.

\subsection{Requisiti ad alto livello per la consegna dei messaggi di allerta}
PWS è una generalizzazione ETWS. I requisiti sono definiti nel
documento del 3GPP \emph{TS 22.268} \cite{3gppts22268}.
La lista seguente fornisce i requisiti ad alto livello per ciò che riguarda
la consegna dei messaggi di allerta.
\begin{itemize}
  \item Il sistema deve essere in grado di inviare i messaggi a più
        utenti contemporaneamente senza che ci sia alcun tipo di feedback
        sull'effettiva consegna
  \item Il sistema deve essere in grado di inviare simultaneamente messaggi
        diversi
  \item I messaggi devono poter essere inviati ad un'area specifica,
        chiamata \textit{Notification Area}, che viene
        specificata dal Warning Notification Provider
  \item I dispositivi abilitati alla ricezione dei messaggi di allerta
        devono essere in grado di ricevere i messaggi anche se sono in modalità
        \emph{idle}
  \item Il sistema deve essere in grado di inviare i messaggi nella lingua
        desiderata, come richiedono le leggi dei Paesi in cui i messaggi
        vengono consegnati
  \item I messaggi vengono processati con politica \gls{fifo}
  \item La ricezione e la presentazione di un messaggio di allerta non
        deve sospendere una chiamata in corso o l'utilizzo dei dati
  \item L'utilizzo di questa tipologia di messaggi deve essere limitata
        alle emergenze dove ci sia pericolo di vita e sia necessario
        intraprendere qualche tipo di azione. Ciò non proibisce che
        l'operatore possa utilizzare la tecnologia broadcast a fini commerciali.
\end{itemize}

\subsection{Contenuto dei messaggi}
Il sistema non deve modificare o tradurre il contenuto di un messaggio; deve
consegnarlo così come è stato ricevuto dal \textit{Warning Notification
Provider}. Un messaggio deve contenere le seguenti 5 informazioni:
\begin{itemize}
  \item Descrizione dell'evento
  \item Area interessata
  \item Azioni da intraprendere
  \item Tempo di scadenza del messaggio (con fuso orario specificato)
  \item Agenzia inviante
\end{itemize}
Ulteriori informazioni possono essere incluse in base ai requisiti normativi.
I messaggi non devono contenere \gls{url} di indirizzi Web, numeri telefonici o
altre informazioni che possano in qualche modo sovraccaricare nell'immediato
l'infrastruttura dell'operatore telefonico. Si presume che, nel caso di
avvenimento che richieda l'invio di un messaggio di allerta, ci sia già
abbastanza traffico e quindi è il caso di non incentivare l'utilizzo della
rete, in modo da evitare congestione.

\subsection{Supporto per i Warning Notification Provider}
Gli operatori devono almeno supportare le funzionalità elencate in
seguito per quanto riguarda l'interazione con le autorità autorizzate
all'invio dei messaggi.
\begin{itemize}
  \item Attivazione della consegna di un messaggio di allerta
  \item Annullamento della consegna di un messaggio di allerta (con ciò si
        intende la possibilità di fermare l'invio in broadcast di un
        messaggio)
  \item Aggiornamento di un messaggio di allerta, per revisionare le
        informazioni fornite in un messaggio precedente
\end{itemize}

\subsection{Requisiti dei dispositivi}
I dispositivi devono ricevere e presentare i messaggi di allerta nella
lingua in cui sono stati consegnati al CBC dal Warning Notification Provider.
Per le leggi in vigore in un determinato Paese potrebbe essere necessario che
i messaggi siano inviati in più lingue; tuttavia non dovrebbe essere
necessaria una traduzione da parte dell'infrastruttura dell'operatore oppure
da parte dello stesso dispositivo.
Deve essere possibile che i messaggi vengano visualizzati sul dispositivo
non appena arrivati senza che ci sia interazione da parte dell'utente del
dispositivo.
Inoltre deve essere possibile che gli utenti configurino il
comportamento del dispositivo riguardo la ricezione dei messaggi;
deve essere almeno data la possibilità di regolare l'intensità del volume alla
ricezione di un messaggio.
I dispositivi devono avere un modo dedicato e distinguibile per notificare la
ricezione di un messaggio di allerta: il suono emesso e il modo di vibrare
deve essere riconoscibile e distinguibile da altri suoni di avviso.
L'interfaccia grafica del dispositivo deve essere fatta in modo che
l'utente possa interrompere il segnale di avviso o la vibrazione;
fino a che non c'è l'intervento manuale dell'utente il dispositivo deve
continuare a suonare e vibrare. Non ci sono specifiche per il tipo di suono e
la cadenza di vibrazione: i dettagli di implementazione sono lasciati al
produttore del dispositivo. Anche la frequenza e la durata del suono è
lasciata all'implementazione; in ogni caso non deve essere possibile influire
o poter silenziare la ricezione di un messaggio di allerta ricevuto
successivamente. Il dispositivo deve automaticamente scartare messaggi
duplicati: un duplicato è un messaggio che è già arrivato al dispositivo ed
è riconoscibile grazie ad un parametro univoco all'interno del messaggio,
come vedremo successivamente.
Il dispositivo non deve poter inoltrare il messaggio ricevuto, non deve poter
rispondere e non deve fornire la possibilità di potere copiare e incollare il
contenuto del messaggio: sono tutte precauzioni per far in modo di evitare
una congestione della rete. Infine, i dispositivi devono aver la possibilità
di memorizzare e visualizzare i messaggi ricevuti se l'utente lo
richiede, e dovrebbero essere abilitati a ricevere messaggi di allerta
simultanei.
La batteria dei dispositivi non deve essere consumata in maniera
significativa a causa della ricezione di questo tipo di messaggi.
All'utente deve essere data la possibilità di poter decidere quali messaggi
visualizzare e quali no, in base alle leggi in vigore nel Paese o alle
politiche decise dall'operatore.
Infine, l'utente dove poter decidere in che lingua visualizzare il
messaggio e se visualizzarlo in più lingue, sempre facendo riferimento alle
leggi del Paese in cui viene erogato il servizio.

\subsection{Note su roaming, sicurezza e leggi}
Deve essere consentito che un dispositivo che supporta il sistema PWS
riceva un messaggio di allerta mentre si trova in roaming.
Per quanto riguarda la sicurezza, il sistema deve trasmettere solamente
messaggi che provengono da fonti autorizzate.
Infine, poiché il servizio di PWS offerto dagli operatori può 
essere soggetto alle normative di un determinato Paese, il
servizio offerto può variare da zona a zona.

\subsection{Requisiti di ETWS, CMAS, EU-ALERT e KPAS}
In ETWS i messaggi di allerta devono essere consegnati rispettando i seguenti
requisiti:
\begin{itemize}
  \item La consegna deve avvenire velocemente subito dopo l'avvenimento di un
        terremoto o di uno tsunami. Questi due si propagano molto rapidamente: il
        tempo che trascorre dal momento in cui si verifica un terremoto/tsunami al
        momento in cui viene colpita una zona è molto poco: si tratta di pochi
        secondi o al più di qualche minuto. Per questo i messaggi di allerta
        devono essere consegnati agli utenti che si trovano nelle zone
        interessate il più rapidamente possibile, in modo che possano agire secondo
        indicazioni.
  \item La consegna deve avvenire in maniera accurata. Le notifiche contengono
        informazioni sulle azioni da intraprendere, per esempio evacuazione; quindi
        i messaggi devono essere consegnati con precisione e devono contenere
        informazioni chiare e comprensibili a tutti, compresi stranieri e persone
        che portano handicap.
\end{itemize}
\begin{figure}
  \includegraphics[width=\textwidth]{img/PWS}
  \caption{I messaggi ETWS}
\end{figure}
Per \emph{durata del tempo di consegna} si intende il tempo che intercorre da
quando il messaggio arriva all'infrastruttura dell'operatore al tempo in cui
avviene l'effettiva consegna sul dispositivo. Le \textit{Primary Notification}
dovrebbero essere consegnate entro 4 secondi ai dispositivi che si trovano
nell'area interessata, anche in caso di congestione della rete; le \textit{Secondary
Notification} non hanno requisiti temporali ma devono essere consegnate
agli utenti che si trovano nell'area interessata anche in situazioni di
congestione. Ovviamente i dispositivi che sono fuori portata o che sono
spenti non sono considerati.
Per quanto riguarda ETWS, i requisiti relativi agli elementi informativi che
devono essere contenuti e al volume di dati in una Primary Notification
e in una Secondary Notification sono:
\begin{itemize}
  \item supportare 2 tipologie di allerta, che sono Terremoto e Tsunami
  \item indicare il comportamento del dispositivo nel momento della ricezione
        (suono, vibrazione, visualizzazione testo)
  \item essere distinguibile da altre notifiche generate a scopo di test,
        esercitazione o altri tipi di servizi di notifica
  \item essere inviate in maniera ottimizzata per quanto riguarda tipo e
        volume di dati, ponendo attenzione ai tipi di dispositivi che saranno
        raggiunti
\end{itemize}
Le Primary Notification devono veicolare pochi dati così da
poter essere spediti velocemente in rete; questo significa il minimo di dati 
necessari per indicare l'arrivo imminente di un terremoto o di uno tsunami.
In termini di spazio si tratta di qualche byte di informazione.
Le Secondary Notification possono veicolare un maggior numero di dati,
così da far pervenire del testo, istruzioni audio, mappe che indicano dove
recarsi etc.
Le Primary Notification hanno maggior priorità rispetto alle Secondary
Notification. In caso ci sia contemporaneamente la necessità di trasmettere
Primary Notification e Secondary Notification, la rete dell'operatore deve
essere in grado di trasmettere i messaggi nel giusto ordine.
Per quanto riguarda gli utenti in roaming, la Primary Notification è molto
importante; il dispositivo deve indicare con chiarezza l'arrivo di un
terremoto o di uno tsunami, includendo per esempio un'icona o un'immagine; ci
si aspetta che la Primary Notification sia inviata in una lingua che sia
conosciuta nell'area interessata per cui la visualizzazione di un'icona
potrebbe essere di fondamentale importanza per quegli utenti che non
conoscono la lingua.
CMAS è un sistema PWS nato negli Stati Uniti. Definisce 3 tipi di
notifiche: \textit{Presidenziali}, \textit{Minacce Imminenti}
e \textit{Rapimento di Minori}. I requisiti sono stati definiti da una legge
del 2006; la successiva implementazione nelle reti del 3GPP non ha richiesto
modifiche a quelle che erano le funzionalità già presenti riguardanti 
la funzionalità di Cell Broadcast; tuttavia sono state sviluppate delle
estensioni.
In aggiunta ai requisiti generali elencati precedentemente si aggiungono i
seguenti requisiti:
\begin{itemize}
  \item Devono essere supportate 3 "classi" di messaggi: \emph{Presidenziali},
        \emph{Minaccia Imminente} e \emph{Rapimento di Minori}
  \item I messaggi di tipo Presidenziale devono sempre essere inviati e
        visualizzati dai dispositivi
  \item Se la notifica viene inviata in lingua inglese, deve essere presentata
        nel dispositivo in tale lingua, se la notifica viene inviata in una lingua
        diversa dall'inglese, questa deve essere visualizzata nel dispositivo
        solamente se tale lingua è stata selezionata dall'utente
  \item Un dispositivo non deve dare la possibilità all'utente di poter decidere
        di non ricevere messaggi di tipo Presidenziale; tuttavia per quanto riguarda
        gli altri due tipi di messaggio l'utente ha facoltà di scelta
  \item Se un utente ha deciso di non visualizzare messaggi di tipo
        \emph{Minaccia Imminente} o \emph{Rapimento di Minori}, il
        dispositivo non deve presentare tali notifiche
  \item Se l'utente non ha bloccato le notifiche per una relativa classe di
        messaggi tutte le notifiche relative a quella classe gli devono essere
        consegnate; ciò vale sia per la lingua inglese che per le altre possibili
        lingue selezionate dall'utente
\end{itemize}
\begin{figure}
  \includegraphics[width=\textwidth]{img/CMAS}
  \caption{Architettura di CMAS}
\end{figure}
Il nome per il sistema PWS in Europa è EU-ALERT. Le lettere EU possono
essere rimpiazzate dalle lettere iniziali di un Paese Europeo se questo vuole
sviluppare una sua estensione per andare incontro alle leggi in vigore.
Il sistema EU-ALERT deve supportare tre tipi di notifiche: messaggi di
allerta per avvisare la popolazione in caso situazioni di emergenza, messaggi
di allerta con informazioni specifiche relative alla situazione di emergenza 
e infine messaggi di avviso di rapimento o scomparsa di minori.
Inoltre i messaggi supportano tre livelli di importanza. Gli EU-ALERT di primo
livello non possono essere scartati dall'utente, mentre quelli di livello 2
e 3 sì. Tutti e tre i tipi di messaggio devono essere associati ad un modo
dedicato e distinguibile di presentazione sul dispositivo. Le notifiche di
livello 1 sono compatibili con le notifiche Presidenziali di CMAS; le
notifiche di livello 2 sono compatibili con CMAS Extreme Alerts; infine le
notifiche di livello 3 sono compatibili con CMAS Severe Alert.
I tre livelli di notifica di EU-ALERT non vanno confusi con i tre tipi di
messaggio elencati precedentemente. I messaggi EU-ALERT devono supportare
molteplici lingue. Per mantenere una certa consistenza e supportare il
roaming, i Paesi che adottano EU-ALERT devono rispettare l'utilizzo dei
Messagge Identifier. Ciò significa che un messaggio può essere visualizzato nella
lingua scelta dal dispositivo, dato che ad uno specifico Message Identifier
corrisponde uno specifico messaggio associato ad un evento predefinito. I
messaggi EU-ALERT possono essere consegnati tramite rete GSM, 3G e LTE,
attraverso la tecnologia Broadcast, così come avviene per CMAS.
Anche il sistema coreano KPAS ha i suoi specifici requisiti; devono essere 
supportate due classi di notifiche: classe 0 e classe 1. Le due
classi differiscono per il fatto che le seconde possono essere scartate in
base a scelte dell'utente, mentre le prime no. I messaggi possono essere
grandi fino a 180 bytes che corrispondono a 90 caratteri coreani.

\section{Formattazione del Messaggio di Allerta}
I messaggi di allerta contengono informazioni relative ad eventi di emergenza.
Quando un dispositivo riceve un messaggio di allerta informa l'utente
dell'evento in questione (l'arrivo imminente di uno tsunami per esempio)
mostrando sul display il messaggio di allerta. Un messaggio di allerta
include le seguenti informazioni:
\begin{itemize}
  \item \textit{Message Identifier}: indica il motivo e il tipo di messaggio
        di allerta
  \item \textit{Serial Number}: questa informazione viene utilizzata per
        identificare un cambiamento nel messaggio di allerta con uno
        specifico Message Identifier
  \item \textit{Warning Type}: indica il tipo di disastro (terremoto o tsunami),
 ed indica anche come presentare il messaggio agli utenti
  \item \textit{Warning Message Contents}: questa informazione contiene il
        messaggio per l'utente
  \item \textit{Digital Coding Scheme}: informa il dispositivo circa la 
codifica e l'alfabeto utilizzati nel messaggio
\end{itemize}
Un singolo messaggio deve includere un Message Identifier e un Serial
Number. Il Message Identifier indica il tipo di messaggio di allerta; i tipi
possibili sono elencati nel documento del 3GPP \emph{TS 23.041}
\cite{3gppts23041}. Il Message Identifier è rappresentato con un numero;
l'intervallo che va da 4352 a 4399 è riservato al sistema PWS e l'intervallo
che va da 4400 a 6399 è riservato per future estensioni di esso.
Message Identifier e Serial Number identificano il messaggio di allerta. L'
eNB potrebbe mandare più volte in broadcast lo stesso messaggio e quindi i
dispositivi potrebbero ricevere più volte lo stesso messaggio. In questo caso
il dispositivo può rilevare i duplicati facendo riferimento alla coppia
Message Identifier/Serial Number. Nel dispositivo che riceve la notifica, la
rilevazione del duplicato non è fatta nello strato RRC ma in uno strato
superiore. Lo strato RRC semplicemente inoltra la notifica PWS con i relativi
Message Identifier e Serial Number allo strato superiore. Il Warning Type è
utilizzato solamente nelle notifiche Primarie ETWS ed indica al dispositivo
se il disastro riguarda un terremoto, uno tsunami o qualcos'altro. Dal
momento che lo scopo delle ETWS Primary Notification è la consegna immediata
dell'informazione di emergenza, i messaggi di allerta consegnati in questa
modalità non contengono informazioni dettagliate riguardanti l'evento.
La grandezza del contenuto del messaggio varia da 1 byte a 9600 byte. In ogni
caso 9600 byte sono troppi per essere trasmessi via radio. Quindi lo strato RRC
segmenta in varie parti il singolo messaggio se questo risulta troppo grande.
Le notifiche possono avere al loro interno insiemi di informazioni differenti.
La Tabella \ref{table:msgcontent} mostra cosa è incluso nelle notifiche
ETWS Primary Notification, ETWS Secondary Notification, e CMAS Notification.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{PWS Notification} & \textbf{Information Element} \\
  \hline
  ETWS Primary Notification & Message Identifier \\
  & Serial Number \\
  & Warning Type \\
  \hline
  ETWS Secondary Notification & Message Identifier \\
  & Serial Number \\
  & Warning Message Contents \\
  & Digital Coding Scheme \\
  \hline
  CMAS Notification & Message Identifier \\
  & Serial Number \\
  & Warning Message Contents \\
  & Digital Coding Scheme \\
  \hline
\end{tabular}
\caption{Il contenuto dei messaggi di allerta, nelle sue varie forme}
\label{table:msgcontent}
\end{table}
In una notifica ETWS era opzionale includere un elemento Warning
Security Information; questo era specificato nella Release 8. In ogni caso il
3GPP ha deciso di invalidare questo elemento perché forniva un livello di
sicurezza troppo basso, quindi non è più permesso includere tale elemento nelle
notifiche ETWS Primary Notification.
Il 3GPP ha pianificato di alzare il livello di sicurezza dei messaggi PWS nella
release 11 o successive. Questo livello di sicurezza sarà applicato non solo
alle notifiche ETWS ma anche alle altre notifiche PWS.

\section{Consegna dei Messaggi di Allerta}
In GSM e UMTS il servizio di Cell Broadcast può essere utilizzato per inviare
messaggi di allerta; questo richiede che il servizio di broadcast sia
attivato nel dispositivo mobile. La consegna dei messaggi di warning è simile
alla consegna di normali messaggi di broadcast, ovvero il servizio PWS è
retrocompatibile. Il servizio dà la possibilità che vengano inviati dei
messaggi ai dispositivi presenti in una determinata zona, senza che questi
diano un feedback di ricezione. Il servizio di broadcast lato UE è
configurabile mediante impostazioni della USIM.

\subsection{GSM}
In seguito viene riportato lo scambio di messaggi necessario tra i vari
componenti dell'infrastruttura che consente la trasmissione di un messaggio
PWS nella rete GSM.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/cbc_bts.png}
  \caption{Consegna dei messaggi su rete GSM}
  \label{fig:gsmdelivery}
\end{figure}
Come si può vedere in figura \ref{fig:gsmdelivery} gli attori coinvolti sono
parecchi. Il primo passo necessario è quello riassunto nel punto 1, dove i
MS si registrano attraverso e le BTS sul \emph{core
network} - riportato come
\gls{msc}. Nel punto 2 il CBE manda un messaggio al CBC che
contiene le informazioni sul tipo di allerta, l'area coinvolta, il testo da
trasmettere e la frequenza con cui trasmetterlo. Nel passaggio 3 il
CBC utilizza le informazioni riguardanti l'area coinvolta per inoltrare
il messaggio alle giuste BTS, ed invia loro il messaggio
\emph{Write-Replace}. Nel passaggio 4, la BTS identifica quali sono i
dispositivi a cui inviare il messaggio ed invia un messaggio di \emph{paging}
seguito da un messaggio che contiene le informazioni broadcast. Nel punto 5
il dispositivo segnala all'utente la notifica e aspetta eventuali ulteriori
informazioni testuali da far visualizzare all'utente tramite la
ricomposizione delle pagine di messaggio di un'eventuale Notifica Secondaria.
Al punto 6 \gls{bsc} conferma a BTS l'avvenuta trasmissione del
messaggio; infine al punto 7 CBC conferma a sua volta a CBE.

\subsection{UMTS}
In questa sezione viene riportato lo scambio di messaggi per quanto riguarda
la rete 3G.
In una rete 3G un messaggio è consegnato dal CBE al nodeB attraverso i
protocolli SABP e NBAP tramite CBC e RNC. La rete può aggiornare o fermare un
messaggio con tali protocolli. Le procedure seguenti del protocollo SABP sono
utilizzate per la consegna dei messaggi di allerta tra CBC e RNC:
\begin{itemize}
  \item procedura Write-Replace
  \item procedura Kill
\end{itemize}
Nella figura \ref{fig:umtsdelivery} viene riportato lo scambio di messaggi
necessari per la consegna di un messaggio di allerta in una rete UMTS dove
vengono coinvolti tutti gli attori.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/cbc_nodeb.png}
  \caption{Consegna dei messaggi su rete 3G}
  \label{fig:umtsdelivery}
\end{figure}
Nel passo 1 avviene la registrazione dei dispositivi nel \emph{core network}.
Nel passo 2 CBE invia a CBC le informazioni necessarie per
inviare il messaggio di allerta, tra cui tipo di warning, testo del
messaggio, area interessata, e periodo di tempo. Nel passo 3, utilizzando le
informazioni relative all'area, CBC identifica quali sono gli RNC
a cui inoltrare il messaggio; invia un messaggio \emph{Write-Replace} che 
un parametro che contraddistingue il messaggio di allerta. 
Nel passo 4 RNC identifica quali sono i nodeB a cui
mandare la richiesta di broadcast. Nel passo 5 nodeB riceve il messaggio e 
comincia ad inviarlo in broadcast in maniera continuativa, come indicato 
da CBE; contemporaneamente manda anche un \emph{paging} contenente 
l'indicazione di warning per risvegliare i dispositivi in stato idle.
Nel passo 6 il dispositivo mobile, se configurato per ricevere i messaggi, 
avverte l'utente. Nel passo 7 e 8 viene riportato a CBC e CBE 
l'esito della trasmissione del messaggio.
La procedura che consente di fermare un broadcast è del tutto similare.
In figura \ref{fig:umtsstack} viene riportato lo stack del protocollo
SABP.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/sabp_proto_stack.png}
  \caption{Lo stack del protocollo SABP}
  \label{fig:umtsstack}
\end{figure}

\subsection{LTE}
In una rete 4G/LTE un messaggio è consegnato da CBE a eNodeB attraverso i
protocolli SBc-AP e S1AP via CBC e MME. La rete può aggiornare o
fermare un messaggio tramite gli stessi protocolli.
La procedure seguenti del protocollo SBc-AP sono utilizzate per la consegna
dei messaggi di allerta tra CBC e MME:
\begin{itemize}
  \item procedura Write-ReplaceWarning
  \item procedura StopWarning
\end{itemize}
Le procedure seguenti del protocollo S1AP sono utilizzate per la consegna dei
messaggi di allerta tra MME e eNodeB:
\begin{itemize}
  \item procedura Write-ReplaceWarning
  \item procedura Kill
\end{itemize}
In figura \ref{fig:ltedelivery1} viene riportato lo scambio di messaggi tra
le varie parti perché avvenga la consegna di un messaggio di allerta su rete
LTE.
Nel punto 0 avviene la registrazione del dispositivo mobile sul \emph{core
network}. Nel punto 1 CBE manda un messaggio a CBC contenente 
tipo di warning, testo del messaggio, area interessata e periodo di broadcast. 
Nel punto 2, utilizzando l'informazione relativa all'area, CBC identifica 
quali sono le MME da contattare ed inoltra il messaggio (in caso non sia
specificata l'area, MME inoltra il messaggio a tutti i eNodeB collegati
a MME); CBC include anche il campo che indica se si
tratta di un messaggio che deve essere mandato in broadcast senza
interrompere un precedente messaggio. In 3 MME conferma immediatamente
a CBC che ha iniziato a distribuire il messaggio agli eNodeB. Nel punto 4
CBC può confermare a CBE che la trasmissione broadcast è stata
presa in carico. In 5 MME inoltra agli eNodeB il messaggio. Nel punto 6 
eNodeB risponde a MME con una \emph{Write-Replace response}. Nel punto 7 il
dispositivo segnala la notifica ricevuta. Nel punto 8, nel caso in cui CBC 
abbia richiesto la conferma delle aree in cui il broadcast è terminato,
tale informazione viene inviata da MME a CBC. Infine nel punto 9
 MME tiene traccia dei messaggi consegnati.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/cbc_enodeb_1.png}
  \caption{Consegna dei messaggi su rete LTE}
  \label{fig:ltedelivery1}
\end{figure}
In seguito, in figura \ref{fig:ltedelivery2} viene proposto lo scambio di
messaggi tra i vari componenti quando si vuole interrompere il broadcast di
un messaggio di allerta in corso. Tale iniziativa parte da CBE.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/cbc_enodeb_2.png}
  \caption{Procedura di stop broadcast su rete LTE}
  \label{fig:ltedelivery2}
\end{figure}
In 1 CBE comunica al CBC l'identificativo del messaggio da
fermare, insieme al suo numero seriale. In 2 CBC identifica quali sono
le MME da informare e invia loro una \emph{Stop Warning Request}
contenente l'area interessata; CBC include l'informazione \emph{Stop
Warning Indicator} se vuole avere conferma dell'avvenuto stop nelle celle. In 3
MME informa immediatamente il CBC che ha cominciato a distribuire
agli eNodeB il messaggio. In 4, CBC può confermare subito a CBE
di aver inoltrato la richiesta. Nel passo 5, CBC invia agli eNodeB una
\emph{Kill Request} che in 6 ferma il broadcast per il messaggio
richiesto; in caso non siano specificate aree, sono coinvolti tutti gli
eNodeB collegati a MME. Nel passo 7, se precedentemente richiesto
tramite \emph{Stop Warning Indication}, MME informa CBC
sull'effettiva risposta ricevuta dagli eNodeB che può cancellare le
informazioni legate al messaggio con quel seriale. In 8 MME può tenere
lo storico dei messaggi processati.
In figura \ref{fig:ltestack} viene riportato lo stack che rappresenta il
protocollo SBcAP.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/sbcap_proto_stack.png}
  \caption{Lo stack del protocollo SBcAP}
  \label{fig:ltestack}
\end{figure}
Da notare che lo strato immediatamente sotto al livello applicativo in
LTE non è più TCP bensì SCTP.
