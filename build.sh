#!/usr/bin/env bash

mkdir -p build
mkdir -p out
pdflatex -output-directory=build main.tex
makeglossaries -d build main
pdflatex -output-directory=build main.tex
pdflatex -output-directory=build main.tex
cp build/main.pdf out/tesi.pdf
