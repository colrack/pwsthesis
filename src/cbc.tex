\section{Introduzione}
Nel documento TS 23 041 del 3GPP \cite{3gppts23041} viene descritto il
servizio \gls{cbs} nelle reti GSM, UMTS e LTE. Per quanto
riguarda la rete GSM, si tratta della relazione tra \gls{cbc}
e \gls{bss}; l'interfaccia viene descritta nel documento del 3GPP TS 22 003
\cite{3gppts22003} e nel documento 3GPP TS 48 049 \cite{3gppts48049}.
Su rete 3G si parla di relazione tra CBC e RNC; il protocollo è
approfondito nel documento del 3GPP TS 25 419 \cite{3gppts25419} ed è
denominato \gls{sabp}.
Infine per quanto riguarda la rete LTE, si tratta della relazione tra
CBC e MME; il protocollo è SBcAP ed è descritto ampiamente
nel documento del 3GPP TS 29 168 \cite{3gppts29168}.

\section{Descrizione Generale}
Tramite il servizio CBS è possibile inviare messaggi ad un gran numero di
dispositivi, senza però avere la possibilità di conoscere se questi sono
arrivati o meno.
I messaggi sono associati ad aree geografiche, note come \emph{aree di
broadcast}; quando vengono spediti in rete, i messaggi sono associati ad
un'area che l'operatore telefonico ha precedentemente definito e alla quale
sono associate una o più celle. I messaggi di broadcast vengono forniti dai
\gls{cbe}, che sono connessi ai CBC. I messaggi sono poi
inoltrati dai CBC alle celle ed infine raggiungeranno i dispositivi.
Una pagina di un messaggio CBS è composta di 82 ottetti, che nel set di
caratteri di default corrispondono a 93 caratteri. Possono essere specificate
codifiche alternative nel \gls{ie} \textit{Data Coding Scheme}. Un
messaggio può essere formato al massimo da 15 pagine; ognuna di queste pagine
avrà lo stesso \textit{Message Identifier} e lo stesso \textit{Serial
Number}; grazie a queste
due informazioni un dispositivo è in grado di ignorare messaggi duplicati.
La cella invia i messaggi broadcast in maniera ciclica e secondo una
frequenza decisa dal fornitore del messaggio.
Per permettere ai dispositivi di far scegliere all'utente quali messaggi
visualizzare e quali no, ai messaggi viene assegnata una classe che
categorizza il tipo di informazione che contengono e la lingua in cui sono.
Un'infrastruttura può potenzialmente attivare un dispositivo, in modo da
ricevere messaggi di broadcast.
PWS è un sistema che sfrutta il servizio CBS e che permette
alle autorità di inviare messaggi di allerta. Il servizio PWS prende
il nome di \gls{etws}, \gls{cmas}, \gls{kpas} e \gls{eualert} a seconda della
zona geografica e abilita l'invio dei messaggi di allerta attraverso le reti
GSM, UMTS e E-UTRAN. Nel tempo il servizio di Cell Broadcast
si è evoluto; il risultato è che ci sono specifiche che riguardano solo una
determinata tecnologia ed altre che invece sono condivise.
In Italia il solo canale broadcast abilitato è il n. 050 che fornisce
informazioni riguardo alla provincia in cui ci si trova. L'impiego originario
era connesso ad un tipo di tariffazione basata sulla posizione dell'utente:
nel caso specifico, le chiamate effettuate dal proprio distretto di residenza
erano tariffate ad un costo minore; per esempio alla fine degli anni '90
Tim proponeva la tariffa Arancione mentre Omnitel la tariffa City Ricaricabile.
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{img/tariffe_cellbroadcast}
  \caption{Tariffa Arancione Tim e Tariffa City Ricaricabile Omnitel}
\end{figure}

\section{Architettura}
Una panoramica generale di quali sono gli attori coinvolti nel servizio di
Broadcast è data dalla Figura \ref{fig:cbsarch}. Sono presenti le tre
evoluzioni di tecnologia per quanto riguarda la parte RAN: GERAN,
UTRAN e E-UTRAN; tutte sono governate dal CN in cui è
presente il CBC ed in particolare si può notare come in LTE sia
stata introdotta l'intermediazione di MME. Nelle sezioni successive
vediamo brevemente i tre tipi di tecnologia.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/cbs_arch.png}
  \caption{Architettura di CBS sulle reti del 3GPP}
  \label{fig:cbsarch}
\end{figure}

\subsection{GSM}
Sebbene in questo documento di tesi non sia approfondito il Cell Broadcast su
rete GSM, viene qui proposta, nella figura \ref{fig:gsmcbarch} la sua
architettura.
Il protocollo per lo scambio di messaggi tra CBE e CBC, marcato 1
in figura, non è definito dal 3GPP. Il protocollo marcato 2 è descritto
nel documento TS 23 041 \cite{3gppts23041}. Il protocollo marcato 3 è
descritto in TS 48 058 \cite{3gppts48058}. Infine, informazioni sul
collegamento marcato 4 si possono trovare nei documenti del 3GPP TS 44 012
\cite{3gppts44012} e TS 45 002 \cite{3gppts45002}.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/gsm_cb_arch.png}
  \caption{Architettura GSM del Cell Broadcast Service}
  \label{fig:gsmcbarch}
\end{figure}

\subsection{UMTS}
Anche nel caso di UMTS l'interfaccia tra CBE e CBC non è di
competenza del 3GPP. Nella figura si può notare che il CBC comunica
con RNC tramite l'interfaccia Iu-Bc e si avvale del protocollo
SABP, descritto nel documento TS 25 419 \cite{3gppts25419}. Il
CBC fa parte del \emph{core network}.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/umts_cb_arch.png}
  \caption{Architettura UMTS del Cell Broadcast Service}
  \label{fig:umtscbarch}
\end{figure}

\subsection{LTE}
Anche in LTE il CBC è un componente del \emph{core network};
comunica con MME tramite un protocollo denominato SBcAP, descritto
in 3GPP TS 29 168 \cite{3gppts29168}. Il componente MME inoltra i
messaggi alle celle (via eNodeB) tramite il protocollo \gls{s1ap},
descritto in TS 36 413 \cite{3gppts36413}.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/lte_cb_arch.png}
  \caption{Architettura LTE del Cell Broadcast Service}
  \label{fig:ltecbarch}
\end{figure}

\section{Funzionalità di CBE}
La specifiche per le funzionalità di CBE non sono di competenza del
3GPP; tuttavia è dato per scontato che CBE sia responsabile per
tutto ciò che riguarda la formattazione dei messaggi e la divisione del
messaggio in più parti. Nel documento ATIS 0700008 \cite{atis0700008} vengono
descritte le specifiche per l'interfaccia tra CBE e CBC, per
fornire supporto al servizio di broadcast. Lo scopo di tale documento è
definire il protocollo di scambio messaggi tra CBE e CBC
indipendentemente dalla tecnologia utilizzata; ciò vuol dire che CBE non
fa assunzioni su quale sia il tipo di tecnologia di rete sulla quale il
messaggio sarà inviato (GSM, UMTS, LTE).

\section{Funzionalità di CBC}
CBC è un nodo del \emph{core network}. Può essere connesso ad una o
più BSCs/RNCs/MMEs; inoltre può essere connesso a più CBE. Il CBC è
responsabile della gestione dei messaggi di broadcast, in particolare:
\begin{itemize}
\item alloca i numeri seriali
\item modifica e cancella i messaggi arrivati a BSC, RNC e MME
\item fa partire l'invio in broadcast di messaggi di lunghezza fissa (82
ottetti) inoltrandoli a BSC, RNC e MME, aggiungendo il padding se necessario
\item determina a quali celle il messaggio deve essere inviato ed indica
il parametro Geographical Scope per ogni messaggio
\item determina l'istante in cui il messaggio deve cominciare ad essere
inviato in broadcast
\item determina l'istante in cui il messaggio deve cessare di essere
inviato in broadcast, informando BSC, RNC e MME
\item determina la frequenza temporale con cui il messaggio di broadcast deve
essere trasmesso
\item determina il canale (solo GSM) sul quale il messaggio deve essere
trasmesso
\item se si tratta di un messaggio di allerta, lo marca in modo che sia
distinguibile da altri messaggi di broadcast, includendo Area, Warning Type e
Warning Message
\end{itemize}
Il CBC deve essere a conoscenza delle aree e di dove sono collocate le celle
in modo da utilizzare BSC/RNC come \emph{concentrator} che si limita ad
inoltrare i messaggi.

\section{Funzionalità di BSC, RNC e MME}
BSC e RNC possono interfacciarsi con un solo CBC. Un BSC invece può
interfacciarsi con molte BTS; allo stesso modo un RNC può interfacciarsi con
più di un NodeB. In LTE, MME può interfacciarsi con uno o più CBC; MME
inoltre si può interfacciare con più di un eNodeB.
BSC, RNC e MME fanno parte di tecnologie diverse ma hanno in comune molte
funzionalità:
\begin{itemize}
\item interpretano i messaggi del CBC
\item memorizzano i messaggi del CBC
\item informano immediatamente CBC se i comandi sono stati ricevuti
correttamente
\item informano CBC se i comandi inviati non sono compresi o se non possono
essere eseguiti
\item inoltrano i messaggi ricevuti dal CBC alle giuste celle
(via BTS/NodeB/eNodeB)
\end{itemize}
Vi sono poi delle funzionalità specifiche; per esempio MME può riportare in
maniera asincrona a CBC la lista delle celle su cui il broadcast è stato
completato. Per quanto riguarda funzionalità specifiche di BTS e RNC, il loro
compito è per esempio quello di includere nel messaggio di paging
l'informazione Paging-ETWS-Indicator; per quanto riguarda MME invece questa
informazione non è necessaria ed è sufficiente che esso invii la richiesta
Write-Replace Warning agli eNodeB.

\section{Differenza tra SMS e CBS}
In questa sezione vediamo quali sono le differenze che contraddistinguono il
servizio SMS ed il servizio CBS.
Innanzitutto i due servizi si differenziano per il tipo di trasmissione:
SMS invia messaggi \textit{punto-punto} mentre CBS invia messaggi
\textit{punto-area}. Il numero di telefono nel caso del servizio SMS è
necessario e deve essere conosciuto, mentre il servizio CBS è
indipendente dal numero. Nel servizio CBS solamente i numeri
preregistrati possono ricevere i messaggi, e li riceveranno indipendentemente
dalla posizione in cui si trovano i telefoni; nel servizio CBS invece
il target è un'area geografica e tutti i telefoni in quell'area saranno
notificati. I messaggi SMS sono bidirezionali: ciò significa che gli utenti
possono rispondere direttamente al mittente; ciò non vale per i messaggi in
broadcast, a cui non è consentita una risposta diretta. In caso di
congestione, il funzionamento del servizio SMS può essere compromesso e
i messaggi possono essere messi in code e subire ritardo nella
consegna, mentre il servizio CBS non soffre di queste problematiche. La
lunghezza di un messaggio SMS è prefissata a 140 o 160 caratteri,
mentre nel caso del CBS questa è prefissata a 93 caratteri; in entrambi
i casi è previsto il concatenamento di messaggi/pagine. Altro punto
fondamentale è la sicurezza: nel servizio SMS il mittente non può
essere verificato; in CBS invece solo l'operatore telefonico è
abilitato ad inviare i messaggi. In SMS non è previsto un modo per
evitare di ricevere i messaggi a seconda di un determinato filtro, mentre in
CBS questa possibilità è prevista; inoltre il servizio di broadcast deve
essere abilitato sul dispositivo in caso si voglia ricevere i messaggi
(tuttavia alcuni messaggi PWS non possono essere ignorati). Nel
servizio SMS esiste la possibilità di richiedere la conferma di
ricezione mentre questo non vale per i messaggi in broadcast (non si può
sapere se un dispositivo ha effettivamente ricevuto il messaggio, è possibile
sapere solamente se il messaggio è in corso di trasmissione). Il messaggio
broadcast può essere ripetuto in un intervallo di tempo che va dai 2 secondi
ai 32 minuti; questa funzionalità non è prevista dal servizio SMS. I
messaggi broadcast possono essere inviati in lingua, in particolare nella
lingua del dispositivo; nei messaggi SMS invece la lingua è la stessa
per tutti i riceventi. Per SMS è prevista la memorizzazione dei
messaggi sul dispositivo/SIM mentre per il servizio CBS ciò è
lasciato al dispositivo e dipende dalla sua implementazione.
Quindi perché non si usa il servizio SMS invece del servizio CBS
per erogare i messaggi di PWS? Nonostante il servizio SMS sia
ampiamente diffuso e supportato da tutti i telefoni, i punti a suo sfavore
sono molteplici. Nell'elenco seguente sono riportati i motivi per cui il
servizio CBS è utilizzato:
\begin{description}
\item[tempistiche:] inviare un messaggio ad un milione di dispositivi
richiede troppo tempo con il servizio SMS; la rete wireless ha una
capacità limitata. Inviare in broadcast un messaggio tramite CBS e
raggiungere milioni di dispositivi è questione di secondi
\item[congestione:] il servizio SMS soffre di tale problema in caso di
utilizzo massiccio o picchi di traffico; CBS non soffre di tali
problematiche
\item[area geografica:] il servizio SMS non prevede di conoscere la
posizione dei dispositivi, quindi sarebbe necessario prima sapere dove si
trovano
\item[privacy:] per utilizzare il servizio SMS sarebbe necessario
disporre di un servizio \gls{lbs} che tenga traccia di dove sono i
dispositivi; con il servizio CBS invece non si conosce l'identità di chi
effettivamente riceve il messaggio, si sa solo che se il dispositivo è sotto
copertura di un determinata cella (coinvolta nell'invio del messaggio
broadcast) riceverà il messaggio
\item[sicurezza:] i messaggi SMS possono essere camuffati e un
criminale potrebbe impersonarsi in un'autorità; questo non vale per
CBS, dove solamente l'operatore telefonico è abilitato all'invio
\item[distinzione:] i messaggi SMS sarebbero uguali ad altri messaggi
ed andrebbero mischiati con essi, in quanto non è previsto un servizio di
priorità; i messaggi potrebbero essere ignorati dall'utente o potrebbe non
essergli dato il giusto peso. I messaggi CBS invece sono distinguibili,
hanno suoneria e vibrazione diversi e possono richiedere di eseguire una
determinata azione
\item[costi:] un servizio di PWS costerebbe molto di più a livello di
infrastruttura se erogato tramite SMS, mentre l'investimento di un
operatore telefonico per abilitare CBS dovrebbe essere facilmente
accessibile
\end{description}
