Durante il periodo di stage è stato sviluppato un Cell Broadcast Center
capace di erogare il servizio su reti 3G e LTE. Come visto nei capitoli
precedenti, il CBC è un'entità a sé stante e fa parte del \emph{core network}.

\section{Vincoli e scelte}
Attualmente nel sistema PriMo tutti i componenti del \emph{core network} sono
implementati in C. Il sistema operativo di riferimento è Linux,
distribuzione Debian. La scelta di utilizzare Linux come sistema operativo e
C come linguaggio di programmazione è dovuta a diversi fattori.
Linux è un sistema operativo con solide basi, con una storia
decennale e con centinaia di sviluppatori attivi. Un motivo non trascurabile
è il fatto che il kernel Linux offre il supporto ai socket SCTP; in altri
sistemi (come visto nella Sezione \ref{sec:sctp} del Capitolo \ref{chap:net})
questo protocollo è considerato di secondaria importanza.
La maggior parte dei linguaggi di programmazione e framework portabili
offrono API ad alto livello per la programmazione dei socket; tuttavia, essendo
SCTP un protocollo non disponibile in tutti i sistemi operativi, sono
veramente pochi i linguaggi che lo supportano. Per esempio in Java
7 sono state introdotte delle API per utilizzare SCTP, ma tali interfacce
sono di fatto utilizzabili solo se il sistema operativo sottostante fornisce il
supporto. \`E possibile in linea teorica scrivere dei binding per un
determinato linguaggio di programmazione o di scripting, ma tale interfaccia
sarebbe comunque dipendente da codice scritto in C e vincolata ad un
determinato sistema operativo. Altra cosa da non sottovalutare è il 
fatto che la maggiorparte dei messaggi
scambiati tra i nodi è codificata tramite specifiche ASN.1. Per la
quasi totalità dei progetti odierni, in un mondo dominato dal web, comunicare
in tale formato non è nemmeno considerato; di conseguenza l'offerta
per quanto riguarda strumenti di codifica/decodifica è tutt'altro che
ampia. Si tratta di un mercato di nicchia: l'offerta di prodotti
\textit{closed source} con supporto commerciale non manca ma le
implementazioni \textit{open source} di fatto sono pochissime e non si
avvicinano minimamente alle funzionalità e alle prestazioni offerte dai
prodotti commerciali. Implementare uno strumento per gestire ASN.1 è fuori
dagli obbiettivi di Athonet, che si affida quindi ad una soluzione
commerciale implementata in C.
Altro fattore di fondamentale importanza a giustificare la scelta del
linguaggio è dettata da esigenze prestazionali: la necessità di gestire in
modo rapido ed efficiente i compiti richiesti e il livello di integrazione
con il sistema operativo (necessaria per un progetto come PriMo) fanno
obbligatoriamente cadere la scelta sul linguaggio C.

\section{Implementazione ed uso delle librerie}
Come accennato nella sezione precedente nell'ambito del progetto è stata
utilizzata una suite di prodotti commerciali per la manipolazione del codice
ASN.1; si tratta della soluzione di OSS Nokalva. Sono compresi compilatore e
librerie runtime da aggiungere in fase di linking agli eseguibili che fanno
utilizzo delle strutture e delle funzioni generate dal compilatore. \`E
possibile eseguire sia un link statico che un link dinamico.
In Figura \ref{fig:ossasn1workflow} si può vedere il flusso di utilizzo degli
strumenti, mentre in Figura \ref{fig:casn1workflow} si può vedere il caso
specifico riguardante il linguaggio C.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/oss-asn1-workflow}
  \caption{Il flusso delle operazioni ASN.1 in Nokalva OSS}
  \label{fig:ossasn1workflow}
\end{figure}
Le definizioni ASN.1 dei messaggi dei protocolli presi in esame sono riportati
all'interno delle specifiche di riferimento rilasciate dal 3GPP.
A partire da queste attraverso il compilatore, sono stati generati due file
per ogni protocollo, un file header .h e un file sorgente .c. Il file header
definisce tutte le strutture necessarie a costruire una \gls{pdu},
il termine con il quale vengono chiamati i messaggi nella documentazione e
nel codice generato. Il file header definisce una notevole
quantità di macro per il preprocessore C che dovrebbero essere d'ausilio allo
sviluppatore nella manipolazione delle strutture, ma che risultano invece
abbastanza complesse nello specifico caso dei due protocolli trattati.
L'utilizzo delle macro ha l'effetto negativo di occultare il tipo di dato
manipolato, portando spesso alla costruzione di strutture improprie senza
generare avvertimenti.
\`E compito dello sviluppatore popolare correttamente le PDU con i dati che
compongono i messaggi. Una volta fatto, la struttura viene passata come
parametro alla funzione della libreria che si occupa di verificare la
correttezza della struttura definita e di convertirla nel formato binario
specificato, pronta per essere spedita; è necessario solamente ricordarsi di
utilizzare le apposite librerie per convertire i valori dall'ordine
significativo per l'host all'ordine significativo per la rete.

In fase di decoding avviene il processo inverso. Da una stringa di valori
binari la funzione di decodifica, previa verifica dei vincoli del messaggio,
restituisce una PDU, che dovrà essere analizzata dalle funzioni di controllo
del protocollo. Ciò significa che una volta arrivato un messaggio al socket,
i dati devono essere passati alla libreria che si occupa di popolare le
strutture dati che rappresentano il messaggio in memoria e che può in seguito
essere facilmente manipolato.
Per facilitare l'utilizzo del codice generato sono state sviluppate delle
funzioni wrapper che riducono notevolmente la verbosità, nascondono molti
dettagli e svolgono lavoro che altrimenti sarebbe ripetitivo.
La maggior parte del lavoro della libreria consiste nel codificare e
decodificare i pacchetti dei protocolli SABP e SBcAP; nonostante la
correttezza sintattica delle strutture dati sia garantita dalla libreria in
fase di codifica/decodifica, la validità semantica e la congruenza dei dati
inseriti nei messaggi non sono assicurate. Sono stati quindi sviluppati dei
componenti atti a testare in maniera automatizzata il corretto comportamento
del software.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{img/c-asn1-workflow}
  \caption{Il flusso delle operazioni ASN.1 in C}
  \label{fig:casn1workflow}
\end{figure}

\section{Applicativo sviluppato}
Sono state sviluppate separatamente due applicazioni che implementano una la
parte che si occupa delle trasmissioni su rete 3G, l'altra quelle su rete LTE.
La prima ad essere sviluppata è stata la seconda, in quanto risulta meno
complessa: infatti come visto nei capitoli precedenti le procedure sono state
notevolmente semplificate e l'obiettivo del servizio si focalizza
esclusivamente sull'erogazione dei messaggi di allerta (non a caso il nome di
\textit{Write-Replace} in UMTS è diventato \textit{Write-Replace Warning} in
LTE).
L'applicazione sviluppata per la parte LTE ha le funzionalità di un CBC, è di
per sé semplice ed è composta da un'interfaccia grafica rappresentata in Figura
\ref{fig:athcbcltegui}.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/athcbc_lte_gui}
  \caption{L'interfaccia grafica sviluppata per testare il protocollo}
  \label{fig:athcbcltegui}
\end{figure}
Dal menu delle impostazioni è possibile decidere il nodo a cui connettersi ed
in seguito è sufficiente premere un pulsante per inizializzare o interrompere
la connessione.
\`E possibile selezionare la procedura da utilizzare e compilare il form
per includere gli Elementi Informativi che si desiderano spedire andando di
fatto a costruire un messaggio che rispetta il protocollo SBcAP. \`E possibile
inizializzare la trasmissione in broadcast di un messaggio utilizzando la
procedura \textit{Write-Replace Warning}, fermarla utilizzando la procedura
\textit{Stop-Warning} ed inviare manualmente messaggi di tipo \textit{Error
Indication}. Infine è possibile osservare nelle finestre dedicate il log
delle richieste inviate, così come quello delle risposte ricevute.
Sostanzialmente l'applicazione crea un socket di tipo SCTP e si connette a MME;
dopo aver composto il messaggio tramite l'interfaccia grafica le strutture
dati della libreria vengono inizializzate per poi essere passate alla funzione
di codifica che ritorna i dati pronti per essere scritti sul socket.
Complementarmente, quando arrivano dalla rete dei dati di tipo binario sul
socket si assume che questi facciano parte di un messaggio del protocollo e
quindi vengono passati alla libreria che decodifica e popola le strutture dati
relative alla procedura ricevuta. Per completare il lavoro è stato necessario
integrare l'applicativo MME (sviluppato internamente) in modo che gestisse la
ricezione delle procedure e l'inoltro dei messaggi agli eNodeB tramite il
protocollo S1AP.
Il CBC che implementa le specifiche SABP è stato sviluppato separatamente ma
ha goduto delle competenze acquisite nel corso dello sviluppo del precedente
software.
Per avere una maggior integrazione con il core network esistente si è optato
per lo sviluppo di uno strumento che esegue in modalità testuale e assume il
tipico comportamento di un \textit{daemon}. Tale programma espone
un'interfaccia HTTP REST con cui può essere controllato. Per esempio per
inizializzare l'invio di un messaggio di allerta è sufficiente spedire un
payload \gls{json} che rispetta un protocollo proprietario predefinito; tale
payload è interpretato dal CBC ed è utilizzato per costruire un messaggio del
protocollo SABP che viene codificato grazie alla libreria ASN.1 ed è
in seguito pronto per essere spedito in rete.
Diversamente dal caso precedente,
viene gestito un socket di tipo TCP per connettersi a RNC; come visto nei
capitoli precedenti, RNC fa parte di RAN e non è integrato nel core network,
di conseguenza non è stato necessario sviluppare ulteriori interfacce da
integrare nella suite aziendale poiché si fa affidamento a nodi RNC
sviluppati da terze parti.
Il comportamento di un CBC è relativamente semplice: è lui che prende
l'iniziativa di comunicare con RNC e MME e quindi deve solamente rimanere in
attesa di un operatore che chiede di spedire un messaggio; inoltre deve
registrare gli eventi relativi alle risposte ottenute dalle controparti e
agli eventuali errori ricevuti.
Un possibile sviluppo futuro prevede senz'altro l'integrazione di entrambi i
protocolli in un unico applicativo in modo da risultare interoperabile e gestire
le stesse risorse, in particolare l'allocazione dei numeri seriali dei
messaggi; oltre a ciò sarebbe molto utile avere un'interfaccia comune ad un
livello più astratto per gestire l'inoltro di un messaggio broadcast
indipendentemente dal tipo di rete 3GPP alla quale il CBC è agganciato.

\section{Analisi dei protocollo e cattura del flusso}
Durante lo sviluppo sono stati utilizzati alcuni strumenti open-source per
analizzare i pacchetti scambiati dai nodi. In particolare è stato utilizzato
Wireshark, un potentissimo applicativo che permette di vedere tutto il traffico
che passa sulle interfacce di rete del sistema operativo.
Wireshark riconosce tantissimi protocolli, permette il filtraggio e presenta
le informazioni sotto forma di una struttura ad albero di facile comprensione.
Fortunatamente entrambi i protocolli utilizzati per il progetto erano già
supportati al momento dell'implementazione del CBC. Nella Figura
\ref{fig:wiresharksbcapwrreq} si può vedere un messaggio
\textit{Write-Replace-Warning} del protocollo SBcAP; la relativa risposta è
riportata nella Figura \ref{fig:wiresharksbcapwrres}. Sulla sinistra si
possono notare tutti gli elementi che compongono il messaggio. Se sono
riconosciuti da Wireshark significa che con ogni probabilità sono formattati
correttamente e rispettano il protocollo.
\begin{figure}
  \includegraphics[width=\textwidth]{img/wireshark_sbcap_wr_req}
  \caption{Un esempio di traccia sbcap Write Replace Warning Request}
  \label{fig:wiresharksbcapwrreq}
\end{figure}
\begin{figure}
  \includegraphics[width=\textwidth]{img/wireshark_sbcap_wr_res}
  \caption{Un esempio di traccia sbcap Write Replace Warning Response}
  \label{fig:wiresharksbcapwrres}
\end{figure}
Uno strumento molto utile è \textit{sctp\_darn} della suite \gls{lksctpt}.
Tale applicativo permette di emulare un nodo, sia in modalità client che
in modalità server; avvalendosi di questo strumento è possibile inviare
pacchetti senza dover per forza avere a disposizione una controparte che
risponda correttamente, allo scopo di analizzare se il traffico inviato
rispetta il protocollo.
Un altro strumento molto usato per il debug delle reti di computer che
funziona da riga di comando è \textit{tcpdump}. Consente all'utente di
intercettare pacchetti e trasmissioni ad esempio dei protocolli TCP/IP
condotti attraverso una rete alla quale il computer è collegato. A
dispetto del nome funziona anche con protocolli non TCP, come UDP o SCTP.
Inoltre, combinato a Wireshark e ad un tunnel ssh, \textit{tcpdump} permette di
eseguire il monitoraggio del traffico su una specifica interfaccia di rete di
un host remoto.

\section{I test in UTRAN/E-UTRAN}
Come già detto, Athonet si limita a sviluppare il core network di una rete
cellulare che è un'infrastruttura ben più ampia; la parte di accesso radio è
denominata UTRAN in UMTS e EUTRAN in LTE.
In UTRAN sono presenti RNC e nodeB; quest'ultimi comunicano con i telefoni
cellulari. In UTRAN, CBC comunica direttamente con RNC.
In EUTRAN invece sono gli eNodeB a comunicare con i dispositivi mobili; in
questo caso quindi CBC non comunica direttamente con nodi che appartengono
alla parte radio, ma comunica con MME che a sua volta è
collegata agli eNodeB.
Per testare sul campo il CBC sviluppato sono stati utilizzati server e
celle prodotti da Nokia Siemens e Huawei, sia per la parte UMTS che per la
parte LTE.
I telefoni a disposizione su cui sono stati testati i messaggi di broadcast
sono LG Google Nexus 4 e Samsung Galaxy Nexus 4. Entrambi i telefoni montano
il sistema operativo Android. Tale sistema operativo nelle impostazioni di
sistema ha un menu apposito per la gestione dei messaggi di broadcast; in Figura
\ref{fig:androidpwssettings} sono riportati degli screenshot del menu
impostazioni mentre in Figura \ref{fig:androidpwsalert} si può vedere come
vengono presentati dal sistema i messaggi.
\begin{figure}
  \includegraphics[width=\textwidth]{img/android_pws_settings.png}
  \caption{Il menu impostazioni per PWS in Android}
  \label{fig:androidpwssettings}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{img/android_pwsalert.png}
  \caption{Come viene presentato il messaggio di allerta in Android}
  \label{fig:androidpwsalert}
\end{figure}

In iOS durante lo stage non è stato possibile testare la
ricezione di un messaggio in quanto l'introduzione effettiva del servizio nel mercato
americano è avvenuta nel 2012 ed è supportata a partire dalla release iOS 6. In
rete si possono trovare degli screenshot come quello di Figura
\ref{fig:iospwsalert}.
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{img/ios_pwsalert.png}
  \caption{Come viene presentato il messaggio di allerta in iOS}
  \label{fig:iospwsalert}
\end{figure}
Essendo una funzionalità non supportata da tutti gli operatori telefonici, Apple
di default ha oscurato la sezione che permette di personalizzare il servizio,
che dovrebbe apparire nel menu delle notifiche. Tale sezione appare solamente
se il telefono include un \textit{bundle} dell'operatore telefonico che
indica che tale servizio è supportato dalla rete dell'operatore; a rendere le
cose ancora più complicate è il fatto che questo \textit{bundle} deve essere firmato
digitalmente per essere accettato dal sistema operativo. Con un iPhone
sbloccato siamo riusciti a far apparire il menu e vedere come dovrebbe
apparire in lingua italiana, come si vede in Figura \ref{fig:iospwssettings}.
In Italia ancora nessun operatore telefonico supporta il servizio PWS. Al
momento dello stage (2013) i telefoni più recenti supportano le reti LTE che
però non sono ancora state introdotte nel territorio italiano da nessun
operatore telefonico.
\begin{figure}
  \centering
  \includegraphics[width=0.75\textwidth]{img/ios_pwssettings.png}
  \caption{Il menu che consente l'abilitazione alla ricezione ai messaggi in
  iOS}
  \label{fig:iospwssettings}
\end{figure}
