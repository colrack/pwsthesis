In questo capitolo viene approfondito come comunicano tra loro CBC e
RNC su rete UMTS. Il protocollo da essi utilizzato prende il nome
SABP e viene ampiamente discusso in 3GPP TS 25 419 \cite{3gppts25419}.
L'interfaccia sottostante fa parte di un insieme di interfacce denominate
Iu che prende il nome specializzato di \emph{Iu-Bc}, definita in 3GPP TS 25 414
\cite{3gppts25414}.

\section{Interfaccia Iu-Bc}
L'interfaccia Iu-Bc (vedi 3GPP TS 25 419 \cite{3gppts25419}) definisce due
alternative per il trasporto dei dati: la prima è basata su \gls{atm}, la
seconda è basata su IP. La figura \ref{fig:iubcstack} rende in
maniera grafica lo stack del protocollo per le due alternative.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/iubcstack.png}
  \caption{Stack del protocollo su ATM (sinistra) e su IP (destra)}
  \label{fig:iubcstack}
\end{figure}
Per quanto riguarda l'alternativa basata su IP, deve essere supportato
lo scambio di informazioni attraverso il protocollo TCP. Il collegamento
tra i due punti è identificato dal numero di porta TCP insieme
all'indirizzo IP, in particolare: porta tcp sorgente, porta tcp
destinazione, indirizzo ip sorgente e indirizzo ip destinazione.
La porta di destinazione assegnata per i messaggi SABP è la 3452.
Tale porta è utilizzata da entrambe le entità (RNC e CBC)
quando intendono stabilire una nuova connessione tra loro. Se una connessione
è già stata stabilita tra le parti, chi invia un messaggio deve utilizzare la
connessione esistente. La versione IP utilizzata può essere la IPv4 o
la IPv6; la seconda è fortemente raccomandata.

\section{Specifiche tecniche di SABP}
Per quanto riguarda lo scambio di messaggi, vengono definite delle procedure
definite \gls{ep}. Queste procedure si suddividono in 2 tipi: le procedure di
\textbf{Classe 1} e le procedure di \textbf{Classe 2}. Le prime sono procedure
per le quali è prevista una risposta, che può essere di successo o riportare
errore; le seconde sono senza feedback e considerate sempre di successo.
La retrocompatibilità del protocollo, così come la compatibilità con futuri
sviluppi, è garantita dal fatto che i messaggi sono codificati in un modo
standard che non verrà cambiato.
Durante una normale attività, il CBC inizia il trasferimento di
messaggi e RNC risponde. RNC apre una connessione verso CBC
solamente nel caso debba riportare errori o debba riportare un riavvio;
l'entità che inizia una connessione è anche responsabile della sua chiusura.
Ci si aspetta che dagli stack di rete sottostanti sia garantita la consegna
dei messaggi in sequenza. Il protocollo ha una serie di funzionalità, tra cui 
il trattamento dei messaggi, determinare il carico dei canali di broadcast,
resettare il broadcast, trattare gli errori. Queste funzionalità sono
implementate dalle procedure descritte in seguito.
Nel documento di specifica è riportato il comportamento logico che ogni nodo 
deve assumere sia per quanto riguarda lo scambio di messaggi sia per quanto
riguarda la segnalazione di errori.
Sono inoltre riportati alcuni esempi e alcune linee guide che aiutano a
risolvere le ambiguità che possono emergere durante la fase di
implementazione del protocollo.

\section{Elementi informativi}
\label{subsec:umtsie}
Gli elementi informativi scambiati tra le parti sono riportati nelle tabelle
nelle sezioni successive; sono descritti in dettaglio nei documenti 3GPP TS
25 419 \cite{3gppts25419} e 3GPP TS 23 041 \cite{3gppts23041} e la loro
codifica e decodifica avviene tramite regole descritte in ASN.1.
Alle regole ASN.1 si aggiungono alcune semplici regole: gli elementi nel
messaggio devono rispettare un ordine ben preciso, riportato nella specifica; 
inoltre un elemento obbligatorio deve apparire esattamente una sola volta,
mentre gli elementi opzionali al più una.
Ogni elemento informativo può essere \textit{Obbligatorio}, \textit{Opzionale} o
\textit{Opzionale a seconda della versione della specifica implementata}. Ad
ogni elemento informativo è associata un'informazione detta di
\textit{criticità} che può assumere tre valori: \textit{Reject IE},
\textit{Ignore IE and Notify Sender} e \textit{Ignore IE}. Senza entrare
troppo nel dettaglio, se il nodo che riceve un Information Element marcato 
\textit{Reject IE} non è in grado di capirlo, deve rispondere
con un messaggio di errore. Se invece l'Information Element è \textit{Ignore
IE and Notify Sender}, il nodo deve rispondere positivamente e riportare di
non aver compreso parte del messaggio. Infine se un Information Element è
\textit{Ignore IE} e non viene compreso dal ricevente, la comunicazione
procede come se nulla fosse accaduto.

\section{Procedure}
In questa sezione vengono riportate e discusse brevemente le procedure
coinvolte nel protocollo SABP.

\subsection{Write-Replace}
La Write-Replace è una procedura di \textbf{Classe 1}.
L'obiettivo della procedura Write-Replace è quello di iniziare una
trasmissione in broadcast di un messaggio oppure rimpiazzarne una precedente.
La figura \ref{fig:sabpwritereplace} riporta in maniera visuale tale
procedura, sia in caso di successo sia in caso di fallimento.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/sabp_writereplace.png}
  \caption{La procedura Write-Replace in SABP}
  \label{fig:sabpwritereplace}
\end{figure}
A prendere iniziativa è CBC. Gli elementi informativi per il messaggio
creato da CBC sono presentati in Tabella \ref{table:umtswritereplace}.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  New Serial Number & \checkmark \\
  Old Serial Number &  \\
  Service Areas List & \checkmark \\
  Category & \\
  Repetition Period & \checkmark \\
  Number of Broadcasts Requested & \checkmark \\
  Data Coding Scheme & \checkmark \\
  Broadcast Message Content & \checkmark \\
  Warning Security Information & \\
  Paging ETWS Indicator & \\
  Warning Type & \\
  Warning Message Content Validity Indicator & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Write-Replace}
\label{table:umtswritereplace}
\end{table}
La presenza dell'IE \textit{New Serial Number} indica che si tratta di un
nuovo broadcast; la presenza sia di \textit{Old Serial Number} che di
\textit{New Serial Number} indica che si tratta di un aggiornamento di un
precedente messaggio.
RNC utilizza l'IE \textit{Service Areas List} per determinare su quali aree è
necessario iniziare il broadcast. Il messaggio viene identificato
univocamente da \textit{Message Identifier}, una parte del \textit{New Serial
Number} e l'elemento \textit{Service Areas Identifier}, che si trova all'interno
della lista \textit{Service Areas List}.
L'IE \textit{Category} porta informazioni relative alla priorità del
messaggio; questa informazione nel protocollo SBcAP, che vedremo nel capitolo
successivo, è stata tralasciata e la priorità di un messaggio si identifica
tramite il \textit{Message Identifier}; nel documento del 3GPP TS 23 041
\cite{3gppts23041}) è presente la lista dei possibili valori numerici che
questo campo può assumere, con relativo significato.
In \textit{Data Coding Scheme} sono riportati la codifica e l'alfabeto
utilizzati in \textit{Broadcast Message Content}, dove viene riportato
l'effettivo messaggio.
Il messaggio deve essere ripetuto periodicamente rispettando i valori
\textit{Repetition Period} (riportato in secondi) e \textit{Number of
Broadcasts Requested}; se il valore \textit{Number of Broadcasts Requested} è
pari a 0 il messaggio deve essere mandato continuamente in broadcast fino ad
una richiesta di fermo.
L'elemento \textit{Paging ETWS Indicator} indica a RNC la necessità di
risvegliare il dispositivo in quanto si tratta di un messaggio di allerta; il
tipo di allerta è indicato in nell'IE \textit{Warning Type}. Come si può
notare questi due elementi sono opzionali in quanto sono un'estensione
introdotta successivamente del servizio CBS.
In caso di successo, RNC risponde con gli elementi in Tabella
\ref{table:umtswritereplacesuccess}.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  New Serial Number & \checkmark \\
  Number of Broadcasts Completed List & \checkmark \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Write-Replace Response in caso di successo}
\label{table:umtswritereplacesuccess}
\end{table}
In caso di problemi, RNC risponde con gli elementi in Tabella
\ref{table:umtswritereplacefail}.
Ciò può succedere se per esempio RNC non è in grado di soddisfare la
richiesta di broadcast su una o più aree richieste; queste aree vengono
riportate nella lista \textit{Failure List}, mentre quelle che hanno avuto
esito positivo vengono riportate nella lista \textit{Number of Broadcasts
Completed List}.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  New Serial Number & \checkmark \\
  Failure List & \checkmark \\
  Number of Broadcasts Completed List & \checkmark \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Write-Replace Response in caso di errore}
\label{table:umtswritereplacefail}
\end{table}

\subsection{Kill}
Lo scopo della procedura \textit{Kill} è quello di fermare il broadcast di un
messaggio. A prendere iniziativa è CBC (\textit{Core Network}), come si vede
in Figura \ref{fig:sabpkill}.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/sabp_kill.png}
  \caption{La procedura Kill in SABP}
  \label{fig:sabpkill}
\end{figure}
Gli Elementi Informativi sono quelli riportati in Tabella \ref{table:umtskill}.
Alla ricezione di tale messaggio, RNC deve fermare il broadcast di un
messaggio precedentemente inviato, identificato come visto prima da
\textit{Messagge Identifier}, \textit{Old Serial Number} e \textit{Service
Area Identifier}.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  Old Serial Number & \checkmark \\
  Service Areas List & \checkmark \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Kill}
\label{table:umtskill}
\end{table}
La risposta di RNC può essere di successo o di fallimento, gli Elementi
Informativi sono riportati nelle Tabelle \ref{table:umtskillcomplete} e
\ref{table:umtskillfailure} rispettivamente. RNC riporta la lista delle aree
in cui l'operazione è andata a buon fine e la lista delle aree per cui si è
verificato un problema. L'elemento \textit{Criticality Diagnostics} è
opzionale e contiene informazioni relative all'incomprensione o
alla mancanza dell'Elemento Informativo indicato.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  Old Serial Number & \checkmark \\
  Number of Broadcasts Completed List & \checkmark \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Kill Complete}
\label{table:umtskillcomplete}
\end{table}
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  Old Serial Number & \checkmark \\
  Failure List & \checkmark \\
  Number of Broadcasts Completed List & \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Kill Failure}
\label{table:umtskillfailure}
\end{table}

\subsection{Load Status Enquiry}
Lo scopo di questa procedura è quello di ottenere informazioni sulla banda
disponibile su una o più aree. CBC invia il messaggio includendo la lista
delle aree, come da Tabella \ref{table:umtsloadquery}.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/sabp_loadquery.png}
  \caption{La procedura Load Query in SABP}
  \label{fig:sabploadquery}
\end{figure}
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Service Areas List & \checkmark \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Load Query}
\label{table:umtsloadquery}
\end{table}
RNC risponde con la lista degli identificativi delle aree e la loro
rispettiva banda disponibile (in bit/sec).
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Radio Resource Loading List & \checkmark \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Load Query Complete}
\label{table:umtsloadquerycomplete}
\end{table}
In caso di problemi RNC risponde con un messaggio di fallimento dove riporta
la lista delle aree interessate ed eventualmente una lista delle aree per cui
invece il dato è disponibile.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Failure List & \checkmark \\
  Radio Resource Loading List & \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Load Query Failure}
\label{table:umtsloadqueryfailure}
\end{table}

\subsection{Message Status Query}
Questa procedura è utilizzata da CBC per interrogare RNC riguardo lo stato di
un messaggio. CBC può aver utilizzato la procedura \textit{Write-Replace}
per richiedere un broadcast ripetuto; tramite questa procedura ha quindi la
possibilità di conoscere lo stato di un determinato broadcast.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/sabp_statusquery.png}
  \caption{La procedura Message Status Query in SABP}
  \label{fig:sabpstatusquery}
\end{figure}
Il messaggio deve contenere gli Elementi Informativi riportati nella Tabella
\ref{table:umtsmsgstatusquery}, che sono gli elementi che servono per
identificare un messaggio di allerta.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  Old Serial Number & \checkmark \\
  Service Areas List & \checkmark \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Message Status Query}
\label{table:umtsmsgstatusquery}
\end{table}
In caso di risposta positiva RNC riporta le aree interessate e il numero di
volte che il messaggio è stato ripetuto.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  Old Serial Number & \checkmark \\
  Number of Broadcast Completed List & \checkmark \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Message Status Query Complete}
\label{table:umtsmsgstatusquerycomplete}
\end{table}
Se l'operazione fallisce RNC risponde con un messaggio che include la lista
delle aree per cui è stato riscontrato un problema; se disponibili, riporta
opzionalmente anche le aree per cui invece il dato è disponibile.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \checkmark \\
  Failure List & \checkmark \\
  Old Serial Number & \checkmark \\
  Number of Broadcast Completed List & \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Message Status Query Failure}
\label{table:umtsmsgstatusqueryfailure}
\end{table}

\subsection{Reset}
\`E prevista la procedura \textit{Reset} il cui scopo è fermare il broadcast
di tutti i messaggi su una o più aree. A prendere iniziativa è CBC; gli
Elementi Informativi coinvolti sono riportati in Tabella \ref{table:umtsreset}.
\begin{figure}[H]
  \includegraphics[width=\textwidth]{img/sabp_reset.png}
  \caption{La procedura Reset in SABP}
  \label{fig:sabpreset}
\end{figure}
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Service Areas List & \checkmark \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Reset}
\label{table:umtsreset}
\end{table}
Anche in questo caso è prevista in caso di successo una risposta
che contiene una lista di aree su cui l'operazione è avvenuta correttamente
(Tabella \ref{table:umtsresetcomplete}). In caso negativo invece viene
riportata la lista delle aree in cui si è verificato un problema e
opzionalmente la lista di aree su cui invece il reset è avvenuto correttamente
(Tabella \ref{table:umtsresetfailure}).
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Service Areas List & \checkmark \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Reset Complete}
\label{table:umtsresetcomplete}
\end{table}
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Failure List & \checkmark \\
  Service Areas List & \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Reset Failure}
\label{table:umtsresetfailure}
\end{table}

\subsection{Restart Indication}
Questa procedura è utilizzata da RNC per indicare a CBC che una o più aree
sono pronte a erogare il servizio di broadcast. \`E possibile che un'area
sia diventata operativa o che ci sia stato un riavvio o un'inizializzazione di
RNC.
\begin{figure}[H]
\centering
  \includegraphics[width=0.5\textwidth]{img/sabp_restart.png}
  \caption{La procedura Restart Indication in SABP}
  \label{fig:sabprestart}
\end{figure}
Gli Elementi Informativi sono riportati in Tabella \ref{table:umtsrestart}.
L'elemento opzionale \textit{Recovery Indication} indica se le informazioni
di broadcast precedentemente inviate necessitano di essere ricaricate; se
l'elemento non è riportato si assume che sia necessario reinformare RNC.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Service Areas List & \checkmark \\
  Recovery Indication & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Restart Indication}
\label{table:umtsrestart}
\end{table}

\subsection{Failure Indication}
Grazie a questa procedura RNC informa CBC di un problema che intercorre in una o
più aree.
\begin{figure}[H]
\centering
  \includegraphics[width=0.5\textwidth]{img/sabp_failure.png}
  \caption{La procedura Failure Indication in SABP}
  \label{fig:sabpfailure}
\end{figure}
Dopo la ricezione di un tale messaggio CBC non dovrebbe generare messaggi
\textit{Write-Replace} che coinvolgono le aree segnalate fino a che non
arriva una \textit{Restart Indication} che riporta che quelle determinate
aree sono ora operative.
Gli Elementi Informativi sono riportati in Tabella \ref{table:umtsfailure}.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Service Areas List & \checkmark \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Failure Indication}
\label{table:umtsfailure}
\end{table}

\subsection{Error Indication}
Questa procedura è utilizzata da RNC per riportare a CBC che c'è stato un
problema che non è stato possibile riportare nel messaggio di risposta 
precedente.
\begin{figure}[H]
\centering
  \includegraphics[width=0.5\textwidth]{img/sabp_errorindication.png}
  \caption{La procedura Error Indication in SABP}
  \label{fig:sabperror}
\end{figure}
Gli Elementi Informativi del messaggio sono riportati in Tabella
\ref{table:umtserror}.
\textit{Message Identifier} e \textit{Serial Number} identificano il
messaggio che ha generato il problema.
L'elemento \textit{Cause} può assumere diversi valori, riportati nel
documento di specifica. L'elemento \textit{Criticality Diagnostics} indica se
il problema è riscontrato a causa di uno specifico Elemento Informativo non
compreso da RNC.
\begin{table}[H]
\centering
\begin{tabular}{ |l|c| }
  \hline
  \textbf{Parametro} & \textbf{Obbligatorio} \\
  \hline
  Message Type & \checkmark \\
  Message Identifier & \\
  Serial Number & \\
  Cause & \\
  Criticality Diagnostics & \\
  \hline
\end{tabular}
\caption{Gli Elementi Informativi di Error Indication}
\label{table:umtserror}
\end{table}
